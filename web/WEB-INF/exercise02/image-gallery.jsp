<%@ page import="java.io.File" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: tcro142
  Date: 25/09/2018
  Time: 8:21 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<html>
<head>
    <title>Title</title>
</head>
<body>


<c:forEach var="i" begin="0" end="${imageFileList.size()-1}">
    <section class="images">
    <p>
        <a href="./Photos/${imageFileList[i].getName()}"><img src="./Photos/${imageFileList[i].getName()}"></a><br>
        ${imageCaptionList[i]}<br>

    </p>
    </section>
<br>
</c:forEach>
<%--<c:forEach var="f" items="${imageFileList}">
    <!--<section class="image">-->
            ${f.getName()}

        <!--note uses of getName, gets name of file, then add to /Photos/ in src MAKE SURE NO EXTRA SPACES!!!
    also, difference between ./ and / by itself, any file src for html or jsp IS RELATIVE TO THE HTML OR JSP not absolute, e.g. in Java we need to get context.-->
    <img src="./Photos/${f.getName()}">

    <!--</section>-->
</c:forEach> --%>



</body>
</html>
