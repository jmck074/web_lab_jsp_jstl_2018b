<%@ page import="ictgradschool.web.simplewebapp.model.bioPOJO" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: jmck074
  Date: 27/09/2018
  Time: 12:40 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body>

<div class="jumbotron text-center bg-secondary">
    <h1> Biographies</h1>
    <p> Exercise 3</p>
</div>

<div class="container">
    <% List<bioPOJO> myData = (List<bioPOJO>) request.getSession().getAttribute("pojos");
    for(bioPOJO aPOJO: myData){%>
    <h3> Author Name: <%=aPOJO.getGiven() + " "+ aPOJO.getSurname() %></h3>
    <p><%=aPOJO.getBioContent() %></p>
     <%
    }
    %>

</div>
<div class="jumbotron text-center bg-secondary">
    <p>I don't have any footer content</p>
</div>


</body>
</html>
