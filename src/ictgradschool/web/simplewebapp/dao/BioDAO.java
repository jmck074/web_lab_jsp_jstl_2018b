package ictgradschool.web.simplewebapp.dao;

//more or less copying from ArticleDAO

import ictgradschool.web.simplewebapp.model.bioPOJO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BioDAO implements AutoCloseable{
    private final Connection conn;

    public BioDAO() throws SQLException {
        this.conn = HikariConnectionPool.getConnection();
    }

    public List<bioPOJO> getAllBios() throws SQLException{
        System.out.println("Calling get info back from database");
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM jstlPractice")){
            //ResultSet is a java.SQL object, specific to SQL queries I guess
            try(ResultSet myResults = stmt.executeQuery()){
                List<bioPOJO> bios = new ArrayList<>();
                //as long as their are entries in the ResultSet I got back from my SQL database
                while(myResults.next()){
                    //then create a POJO with this data by calling another method, and add the returned object to my bios List
                    bios.add(biographyFromResultSet(myResults));
                }
                return bios;
            }
        }
    }

    public void addBioToDB(String fname, String lname, String bio) throws SQLException {
        try (PreparedStatement stmt = this.conn.prepareStatement("INSERT INTO jstlPractice (fname,lname,bio) VALUES (?,?,?)")){
            stmt.setString(1,fname);
            stmt.setString(2,lname);
            stmt.setString(3,bio);

            stmt.executeUpdate();

        }
    }
    //This is the method that takes the SQL data (provided by tge getAllBios method and puts it into a POJO or normal java object
    private bioPOJO biographyFromResultSet(ResultSet someResult) throws SQLException{
        return new bioPOJO(someResult.getInt(1),someResult.getString(2),someResult.getString(3),someResult.getString(4));
    }
    @Override
    public void close() throws SQLException {
        this.conn.close();
    }
}
