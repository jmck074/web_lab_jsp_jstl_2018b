package ictgradschool.web.simplewebapp.exercise03;

import ictgradschool.web.simplewebapp.dao.BioDAO;
import ictgradschool.web.simplewebapp.model.bioPOJO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class Biography_v2 extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        System.out.println("Biography Version 2");

        //get info from form... don't forget to map xml (in this case probably just change xml to point here instead of exercise01.Biography
        String fname = request.getParameter("firstname");
        String lname = request.getParameter("lastname");
        String biography = request.getParameter("biography");
        if(biography.contains("<")){biography=biography.replace('<',' ');}

        //Now need to get this info into the SQL database... I think... via DAO?
        try{
            BioDAO myDataAccessObject = new BioDAO();
            //call method to get info into SQL?
            myDataAccessObject.addBioToDB(fname,lname,biography);
            System.out.println("Data should have been added to the database");
            //call method to get infor back OUT OF database?
            List<bioPOJO> allMyData = myDataAccessObject.getAllBios();
            request.getSession().setAttribute("pojos", allMyData);

        }catch(SQLException e){
            System.out.println("SQL Exception");
        }
        //Need to add redirect here?
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/exercise03/Biography_v2.jsp");
        dispatcher.forward(request, response);




    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{doGet(request, response);
    }
}
