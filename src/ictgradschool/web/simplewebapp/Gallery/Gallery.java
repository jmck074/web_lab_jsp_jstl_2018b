package ictgradschool.web.simplewebapp.Gallery;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Gallery extends HttpServlet {

    //TODO setup the photo directory as an instance variable
    private File photoDirectory;
    private List<File> images;
    private List<String> imageCaptions;
    public void init() {
        //TODO get the photo directory and store it in the instance variable you created
        this.photoDirectory = new File(getServletContext().getRealPath("/Photos"));
        System.out.println(getServletContext().getRealPath("/Photos").toString());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Inside Gallery Servlet doGet method...");

        //TODO use the getFileDataList method to get all images
        images = getFileDataList(photoDirectory);
        imageCaptions = getNames(images);
        System.out.println("Size of images "+ images.size());
        //TODO create a session attribute to store all images
        req.getSession().setAttribute("imageFileList", images);
        req.getSession().setAttribute("imageCaptionList", imageCaptions);
        //TODO call the displayGallery method to display the image gallery
        displayGallery(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    private List<File> getFileDataList(File folder) {
        //TODO create a file list to store all image files
        List<File> imageList = new ArrayList<>();
        //TODO use a loop to loop through all image files and add them to the list
       // File [] images = photoDirectory.listFiles

        for(File f: folder.listFiles()){
           imageList.add(f);
       }
        //TODO return the list of images
        return imageList;
    }

    private List<String> getNames(List<File> iFiles){
        List<String> names = new ArrayList<>();
        for (File f: iFiles){
            String name = f.getName();
            name= name.replace('_',' ');
            name = name.replaceAll(".jpg","");
            names.add(name);
        }
        return names;
    }
    private void displayGallery(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //TODO use dispatcher.forward to forward to 'image-gallery.jsp'

        //note / at start of address path. Also first folder is WEB-INF, don't have to .. out of Gallery folder or cd into web
        String nextJSP = "/WEB-INF/exercise02/image-gallery.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        System.out.println("Did this print? I have a RequestDispatcherObject");
        dispatcher.forward(request, response);
        System.out.println("This is supposed to print after dispatcher.forward");
    }

}
