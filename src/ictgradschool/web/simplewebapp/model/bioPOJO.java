package ictgradschool.web.simplewebapp.model;

//Represents a bio for exercise 3
public class bioPOJO {
    private Integer id;
    private String given;
    private String surname;
    private String bioContent;

    public bioPOJO(Integer id, String given, String surname, String bioContent){
        this.id=id;
        this.given=given;
        this.surname=surname;
        this.bioContent=bioContent;
    }

    public bioPOJO(String given, String surname, String bioContent){
        this.given = given;
        this.surname = surname;
        this.bioContent=bioContent;
    }

    public Integer getId(){return id;}
    public void setId(Integer id){this.id=id;}

    public String getGiven() {
        return given;
    }

    public void setGiven(String given) {
        this.given = given;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getBioContent() {
        return bioContent;
    }

    public void setBioContent(String bioContent) {
        this.bioContent = bioContent;
    }
}
